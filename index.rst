.. |label| replace:: 1st Vision Anzeige der Artikelnummer für Variantenartikel
.. |snippet| replace:: FvVariantOrderNumber
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.4.3
.. |maxVersion| replace:: 5.5.8
.. |version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Auf der Artikeldetailseite wird die Artikelnummer des Masterartikel ausgeblendet, erst wenn eine Variante ausgewählt wurde, wird die Artikelnummer eingeblendet.
Mit Hilfe eines Schalter kann man in der Kaufabwicklung die Artikel hinzufügen-Funktion aktivieren und deaktivieren.


Backend
-------

:Anzeige Artikelnummer bei Bestellabschluss: Hier können Sie das Eingabefeld aktivieren oder deaktivieren

Textbausteine
_____________
keine

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/checkout/cart_footer.tpl:
:/detail/content/buy_container.tpl:


